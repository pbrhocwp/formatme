all: release

release:
	yarn shadow-cljs release app
	mkdir -p public
	cp assets/* target/main.js public/
	@echo Files are ready in the "public" directory

dev:
	mkdir -p target
	cp assets/* target/
	yarn shadow-cljs watch app

clean:
	rm -rf target public
