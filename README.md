# formatme

A very simple parser and formatter

## Building
make

**or**

yarn  
yarn shadow-cljs release app  
cp assets/* target/  
Open target/index.html in a browser.

## Building for development
make dev

**or**

yarn  
yarn shadow-cljs compile app  
cp assets/* target/  
yarn shadow-cljs watch app  
Open a browser at http://localhost:8080
