(ns app.utils)

(defn is-char [c]
  (fn [s]
    (or (= s "") (= (first s) c))))

(defn is-not-char [c]
  (fn [s]
    (or (= s "") (not= (first s) c))))

(defn eat-until
  "Consume the string s until pred return true.
pred is a function tacking s has a parameter.
eat-until return an array with the collected result string and the remaining string"
  [s pred]
  (loop [s s acc ""]
    (if (pred s)
      [acc s]
      (if (= (first s) \\)
        (recur (subs s 2) (str acc (second s)))
        (recur (subs s 1) (str acc (first s)))))))

(defn skip-spaces [s]
  (second (eat-until s (is-not-char " "))))
