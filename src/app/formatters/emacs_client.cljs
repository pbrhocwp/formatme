(ns app.formatters.emacs-client
  (:require [app.components.cspan :refer [cspan]]
            [clojure.string :as str]
            [app.utils :as u]
            [app.formatters.json :as json]
            [app.components.clipboard :as clipboard]))

(defonce pretty-print (atom false))

(defn find-options [input opt]
  (filter #(and (vector? %) (= (first %) opt)) input))

(defn find-host [input]
  (str/replace (or (first
                    (remove #(= % "curl")
                            (filter string? input)))
                   "")
               #"^[\"']|[\"']$"  ""))

(defn find-method [input]
  (second (first (find-options input "-X"))))

(defn find-headers [input]
  (map #(let [[header value] (u/eat-until (second %) (u/is-char \:))]
          [(str header ":") (subs value 1)])
       (find-options input "-H")))

(defn find-data [input]
  (map second (find-options input "-d")))

(defn format [output render]
  [:div {}
   [:p {:style {:cursor :pointer}
        :on-click (fn [_] (swap! pretty-print not) (render))}
    (if @pretty-print \u2611 \u2610) " Pretty"]
   (cspan (find-method output) "magenta") " " (cspan (find-host output) "blue") [:br]
   (for [[h v] (find-headers output)]
     [:span {:key (gensym)}
      (cspan h "darkorange") " " (cspan v "grey")  [:br]])
   (map (fn [x]
          [:p {:key (gensym)}
           ((if @pretty-print json/json->hiccup identity) x)])
        (find-data output))])

(defn copy [output]
  (clipboard/copy-to-clipboard
   (str
    (find-method output) " " (find-host output) "\n"
    (apply str (map (fn [[h v]] (str h " " v "\n")) (find-headers output))) "\n"
    (apply str (map (if @pretty-print json/json->pretty-print identity) (find-data output))) "\n")))



