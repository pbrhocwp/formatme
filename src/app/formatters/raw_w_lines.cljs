(ns app.formatters.raw-w-lines
  (:require [app.components.cspan :refer [cspan]]))

(defn format [output render]
  [:div {}
   (for [l output]
     (if (vector? l)
       [:p (cspan (first l) "magenta") " " (cspan (second l) "blue")]
       [:p (cspan l "green")]))])
