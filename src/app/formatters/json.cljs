(ns app.formatters.json
  (:require [clojure.string :as str]))

(defn json->clj [s]
  (js->clj (.parse js/JSON s)))

(defn indent [ind]
  {:key (gensym) :style {:padding-left (str (* ind 25) "px")}})

(declare render)

(defn render-boolean [s ind]
  [:span {:key (gensym) :style {:color "blue"}}
   (str s)])

(defn render-number [s ind]
  [:span {:key (gensym) :style {:color "darkcyan"}}
   (str s)])

(defn render-string [s ind]
  [:span {:key (gensym) :style {:color "brown"}}
   "\"" s "\""])

(defn render-key [s ind]
  [:span {:key (gensym) :style {:color "darkmagenta"}}
   "\"" s "\""])

(defn render-vector [s ind]
  [:span "[" [:br]
   (map (fn [v]
          [:span (indent ind) (render v ind) "," [:br]])
        (butlast s))
   (when-let [v (last s)]
     [:span (indent ind) (render v ind) [:br]])
   [:span (indent (dec ind)) "]"]])

(defn render-map [s ind]
  [:span "{" [:br]
   (map (fn [[k v]]
          [:span (indent ind) (render-key k ind) ": " (render v ind) "," [:br]])
        (butlast s))
   (when-let [[k v] (last s)]
     [:span (indent ind) (render-key k ind) ": " (render v ind) [:br]])
   [:span (indent (dec ind)) "}"]])

(defn render [s ind]
  (cond
    (boolean? s) (render-boolean s ind)
    (number? s) (render-number s ind)
    (string? s) (render-string s ind)
    (vector? s) (render-vector s (inc ind))
    (map? s) (render-map s (inc ind))))

(defn json->hiccup [s]
  (try
    (render (json->clj s) 0)
    (catch js/Error e
      (str e))))

(defn json->pretty-print [s]
  (try
    (.stringify js/JSON (.parse js/JSON s) nil 4)
    (catch js/Error e
      (str e))))
