(ns app.parsers.curl
  (:require [clojure.string :as str]
            [app.utils :as u]))

(defn parse-normal [s]
  (u/eat-until s (u/is-char " ")))

(defn parse-option [s]
  (let [opt (subs s 0 2)
        ns (u/skip-spaces (subs s 2))
        fc (first ns)
        [c of] (if (or (= fc \") (= fc \'))
                 [fc 1]
                 [" " 0])
        [arg nns] (u/eat-until (subs ns of) (u/is-char c))]
    [[opt arg] (subs nns of)]))

(defn do-parse [input acc]
  (case (first input)
    nil acc
    " " (recur (u/skip-spaces input) acc)
    "-" (let [[opt ns] (parse-option input)]
          (recur ns (conj acc opt)))
    (let [[opt ns] (parse-normal input)]
      (recur ns (conj acc opt)))))

(defn parse
  ([input] (parse input []))
  ([input acc] (do-parse (str/replace input #"\n" " ") acc)))
