(ns app.components.toast)

(def ms 2000)
(def displayed (atom false))
(def text (atom ""))
(def renderer (atom identity))

(defn add-toast [render]
  (reset! renderer render)
  [:div {:style {:display (if @displayed "block" "none")
                 :position :fixed :top "1em" :right "1em"
                 :padding-left "20px" :padding-right "20px"
                 :border "1px solid black"
                 :border-radius "10px"
                 :color "white"
                 :background "black"
                 :opacity 0.8
                 :z-index 1000}}
   [:p @text]])

(defn toast [t]
  (reset! text t)
  (reset! displayed true)
  (js/setTimeout (fn [_]
                   (reset! displayed false)
                   (@renderer))
                 ms)
  (@renderer))

