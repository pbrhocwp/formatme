(ns app.components.clipboard
  (:require [app.components.toast :as toast]))

(defn copy-to-clipboard [text]
  (.writeText js/window.navigator.clipboard text)
  (toast/toast "Text copied into the clipboard"))

