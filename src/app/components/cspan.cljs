(ns app.components.cspan)

(defn cspan [s color]
  [:span {:style {:color color}} s])
