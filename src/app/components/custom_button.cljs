(ns app.components.custom-button
  (:require [clojure.string :as str]))

(defn add-button [label click-fn]
  [:p
   [:span {:style {:cursor :pointer}
           :on-mouse-over (fn [e] (set! (.. e -currentTarget -style -backgroundColor) "#ddd"))
           :on-mouse-out  (fn [e] (set! (.. e -currentTarget -style -backgroundColor) "white"))
           :on-click click-fn
           }
    label]])
