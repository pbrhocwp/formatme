(ns app.components.custom-list
  (:require [clojure.string :as str]))

(defn make-list [key title render values]
  (let [idx (js/parseInt (or (.getItem js/localStorage key) "0"))]
    {:key key
     :title title
     :value (nth values idx)
     :idx idx
     :display false
     :render render
     :values values}))

(defn add-button [idx label state value]
  [:div {:key (gensym idx)
         :on-click (fn [_]
                     (swap! state assoc :idx idx :value value :display false)
                     (.setItem js/localStorage (:key @state) idx)
                     ((:render @state)))
         :style {:color (if (= (:idx @state) idx) "blue" "black")
                 :padding "3px" :cursor :pointer}}
   [:label {:style {:display :block :float :left :width "20px"}}
    (if (= (:idx @state) idx) \u2611 \u2610)]
   label])

(defn add-list [w state]
  [:div {:style {:position :relative}}
   [:div {:key (:title @state)
          :style {:display (if (:display @state) "block" "none")
                  :width w
                  :position :absolute
                  :border "1px solid black" :padding "10px"
                  :z-index 1000
                  :background-color "white"}
          :on-mouse-leave (fn [_]
                            (swap! state assoc :display false)
                            ((:render @state)))}
    [:p (:title @state)]
    (map (fn [b idx]
           (add-button idx (:label b) state b))
         (:values @state) (iterate inc 0))]])
