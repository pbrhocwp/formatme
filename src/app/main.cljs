(ns app.main
  (:require [rum.core :as rum]
            [clojure.string :as str]
            [app.components.toast :as toast]
            [app.components.custom-list :as cl]
            [app.components.custom-button :as cb]
            [app.parsers.raw]
            [app.parsers.curl]
            [app.formatters.raw]
            [app.formatters.raw-w-lines]
            [app.formatters.emacs-client]
            [app.formatters.json]))

(defonce app-dom-node (.getElementById js/document "app"))

(declare render)

(def parser-list (atom (cl/make-list "parser" "Select a parser: " #'render
                                     [{:label "raw input" :parser app.parsers.raw/parse}
                                      {:label "curl request"  :parser app.parsers.curl/parse}
                                      {:label "curl response" :x 1}])))

(def formatter-list (atom (cl/make-list "formatter" "Select a formatter: " #'render
                                        [{:label "raw format" :formatter app.formatters.raw/format}
                                         {:label "raw format with lines" :formatter app.formatters.raw-w-lines/format}
                                         {:label "apidoc format"}
                                         {:label "Emacs restclient format" :formatter app.formatters.emacs-client/format
                                          :copy-fn app.formatters.emacs-client/copy}])))

(def input (atom ""))

(defn proceed-input [input]
  (let [parser (:parser (:value @parser-list))
        formatter (:formatter (:value @formatter-list))]
    (cond (and parser formatter) (formatter (parser input) #'render)
          parser (parser input)
          formatter (formatter input #'render)
          true input)))

(defn proceed-copy [input]
  (let [parser (:parser (:value @parser-list))
        copy (:copy-fn (:value @formatter-list))]
    (cond (and parser copy) (copy (parser input))
          copy (copy input))))

(defn sentence-word [txt]
  [:div {:style {:padding "5px" :float :left}} txt])

(defn sentence-button [w lst extra-text]
  [:div {:style {:cursor :pointer :padding "4px" :float :left}
         :on-mouse-move (fn [e]
                          (when-not (:display @lst)
                            (swap! lst assoc :display true)
                            (render)))
         :on-mouse-leave (fn [e]
                           (when (:display @lst)
                             (swap! lst assoc :display false)
                             (render)))}
   [:u {:style {:color "blue" :hover {:color "red"}}}
    [:b (:label (:value @lst))]] extra-text
   (cl/add-list w lst)])

(defn clear-textarea [_]
  (reset! input "")
  (set! (.-value (.getElementById js/document "textarea")) @input)
  (render))

(rum/defc main-page []
  [:div {}
   (toast/add-toast #'render)
   [:div {}
    (sentence-word "Paste your") (sentence-button "150px" parser-list "") (sentence-word "below.")
    (sentence-word "Output formatted as") (sentence-button "250px" formatter-list ".")]
   [:div {}
    [:div {:style {:position :absolute :top "3px" :right "10px"}}
     (cb/add-button "clear" clear-textarea)]
    [:textarea {:id "textarea" :rows 10 :style {:width "100%"}
                :on-change (fn [e] (reset! input (.. e -target -value)) (render))
                :defaultValue @input}]
    (when (:copy-fn (:value @formatter-list))
      [:div {:style {:float :right}}
       (cb/add-button "copy" (fn [_] (proceed-copy @input)))])
    [:div (proceed-input @input)]]])

(defn render []
  (rum/mount (main-page) app-dom-node))

(defn main! []
  (render))

(defn reload! []
  (render))
